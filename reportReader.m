classdef reportReader
    %UNTITLED Summary of this class goes here
    % J. Grelet IRD US191 IMAGO - 2017
    
    properties   (Access = private)
        fileNames
    end
    
    methods % public methods
        function self = reportReader(fileNames, varargin)
            
            % pre initialization - select filename
            if nargin < 1 || isempty(fileNames)
                [fileNames, pathName] = uigetfile({'*.txt','Seabird report (*.txt)'},...
                    'Select files','MultiSelect','on');
                
                if isempty(fileNames)
                    error(message('MATLAB:reportReader:Empty fileName'));
                else
                    fileNames = fullfile(pathName, fileNames);
                end
            end
            
            % post initialization
            if ischar(fileNames)
                fileNames = {fileNames};
            end
            self.fileNames = fileNames;
            
            A ={}; B={}; b = {};
            for f = self.fileNames
                file = char(f);
                [station, s] = read(self, file);
                fprintf(1, 'St %s: ',station);
                b = [b, station];
                for i=1:length(s)
                    fprintf(1,'%s\t', strrep(s(i).sn, sprintf('\r'),''));
                    b = [b, {s(i).sn}];
                end
                fprintf(1,'\n');
                B = [B;b];
                b = {};
            end
            A = [A, 'Station'];
            for i=1:length(s)
                A = [A, {s(i).name}];
            end
            xlswrite('report_all.xls', A, 'CTD reports', 'A1');
            xlswrite('report_all.xls', B, 'CTD reports', 'A2');
            
            
        end % end of constructor
        
        % read files and fill containers.Map
        % -----------------------------------------
        function  [station, sensors] = read(self,file)
            
            fprintf(1, 'read file: %s\n', file);
            fid = fopen(file);
            tline = fgetl(fid);
            while ischar(tline)
                % disp(tline)
                st = regexp(tline, '.*?(\d+)\.xmlcon', 'tokens');
                sensor = regexp(tline,'\s*(\d+)\)\s*(.*)$', 'tokens');
                sn = regexp(tline,'\s+Serial number\s+: (.*)$', 'tokens');
                if ~isempty(st)
                    station = st{1}{1};
                end
                if ~isempty(sensor)
                    i = str2double(sensor{1}{1});
                    name = sensor{1}{2};
                    sensors(i).name = strrep(name, ',',newline);
                    sensors(i).sn = 'N/A';
                end
                if ~isempty(sn)
                    if isempty(sn{1}{1})
                        sensors(i).sn = 'N/A';
                    else
                        sensors(i).sn = sn{1}{1};
                    end
                end
                tline = fgetl(fid);
            end
            fclose(fid);
        end % end of read
        
    end % end of public methods
    
end

