// SbeConReport project SbeConReport.go
package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"runtime"
	"time"
)

const version string = "SbeConReport. Version 0.1  Jgrelet IRD - Cassiopee cruise - R/V Atalante"

var report_path_windows = "M:/AMOP/data-processing/CTD/data/reports"

//var report_file_windows = "M:/CASSIOPEE/Documents/METROLOGIE/cassiopee_ctd_report.txt"

var report_path_unix = "/m/AMOP/data-processing/CTD/data/reports"

//var report_file_unix = "/m/CASSIOPEE/Documents/METROLOGIE/cassiopee_ctd_report.txt"

var regSensorType = regexp.MustCompile(`Frequency (\d), (\w+)`)
var regSensorVoltage = regexp.MustCompile(`A/D voltage (\d), (\w+)`)
var regStation = regexp.MustCompile(`Instrument configuration file:.*(\w{3}\d{5}).xmlcon`)
var regSerialNumber = regexp.MustCompile(`Serial number : (\d+)`)

var sensor string

func main() {

	var report_path string
	//	var report_file string

	// print version
	fmt.Println(version)
	fmt.Println(time.Now().Format(time.RFC850))

	// read TSG track
	if runtime.GOOS == "windows" {
		report_path = report_path_windows
	} else {
		report_path = report_path_unix
	}

	files, _ := filepath.Glob(report_path + "/*")

	for _, file := range files {
		fid, err := os.Open(file)
		if err != nil {
			log.Fatal(err)
		}
		defer fid.Close()

		scanner := bufio.NewScanner(fid)
		for scanner.Scan() {
			str := scanner.Text()
			// fmt.Println(str)
			match := regStation.MatchString(str)
			if match {
				res := regStation.FindStringSubmatch(str)
				// fmt.Println(res)
				value := res[1]
				fmt.Printf("\n\nStation: %s\n", value)
			}
			match = regSensorType.MatchString(str)
			if match {
				res := regSensorType.FindStringSubmatch(str)
				//fmt.Println(res)
				value := res[1]
				sensor = res[2]
				fmt.Printf("%s:\t%s ", sensor, value)
			}
			match = regSensorVoltage.MatchString(str)
			if match {
				res := regSensorVoltage.FindStringSubmatch(str)
				//fmt.Println(res)
				value := res[1]
				sensor = res[2]
				fmt.Printf("%s:\t%s ", sensor, value)
			}
			match = regSerialNumber.MatchString(str)
			if match {
				res := regSerialNumber.FindStringSubmatch(str)
				// fmt.Println(res)
				value := res[1]

				fmt.Printf("S/N: %s\n", value)

			}
		}

		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
	}
	fmt.Printf("\n\n%d files read ...\n", len(files))
}
